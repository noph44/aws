resource "aws_internet_gateway" "development_igw" {
  vpc_id = aws_vpc.development.id

  tags = {
    Name = "development"
  }
}