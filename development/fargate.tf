resource "aws_ecs_cluster" "development_ecs_cluster" {
  name               = "development-fargate-cluster"
  capacity_providers = ["FARGATE"]
}