resource "aws_acm_certificate" "development_cert" {
  domain_name       = "*.dev.thenoph.com"
  validation_method = "DNS"
}

resource "aws_acm_certificate_validation" "development_cert_validation" {
  certificate_arn         = aws_acm_certificate.development_cert.arn
  validation_record_fqdns = [for record in aws_route53_record.development_cert_validation_record : record.fqdn]
}