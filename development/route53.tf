data "aws_route53_zone" "primary_zone" {
  zone_id = "ZB0FKTLM94EBG"
}

resource "aws_route53_zone" "development_zone" {
  name = "dev.thenoph.com"

  tags = {
    Environment = "dev"
  }
}

resource "aws_route53_record" "developnent_nameservers" {
  zone_id = data.aws_route53_zone.primary_zone.zone_id
  name    = "dev.thenoph.com"
  type    = "NS"
  ttl     = "30"

  records = [
    "${aws_route53_zone.development_zone.name_servers.0}",
    "${aws_route53_zone.development_zone.name_servers.1}",
    "${aws_route53_zone.development_zone.name_servers.2}",
    "${aws_route53_zone.development_zone.name_servers.3}",
  ]
}

resource "aws_route53_record" "development_load_balancer_record" {
  zone_id = aws_route53_zone.development_zone.zone_id
  name    = "*.dev.thenoph.com"
  type    = "A"

  alias {
    name                   = aws_lb.development_alb.dns_name
    zone_id                = aws_lb.development_alb.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "development_cert_validation_record" {
  for_each = {
    for dvo in aws_acm_certificate.development_cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = aws_route53_zone.development_zone.zone_id
}