output "vpc" {
  value = aws_vpc.development
}

output "public_subnet_az1" {
  value = aws_subnet.development_public_az1
}

output "public_subnet_az2" {
  value = aws_subnet.development_public_az2
}

output "private_subnet_az1" {
  value = aws_subnet.development_private_az1
}

output "private_subnet_az2" {
  value = aws_subnet.development_private_az2
}

output "lb_listener" {
  value = aws_lb_listener.development_lb_listener
}

output "lb_sg" {
  value = aws_security_group.development_alb_sg
}

output "fargate_cluster" {
  value = aws_ecs_cluster.development_ecs_cluster
}