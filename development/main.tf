provider "aws" {}

terraform {
  backend "remote" {
    organization = "ParadigmTechnologies"

    workspaces {
      name = "aws-development"
    }
  }
}

data "aws_caller_identity" "identity" {}

data "aws_availability_zones" "available" {
  state = "available"
}