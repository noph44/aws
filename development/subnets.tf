resource "aws_subnet" "development_public_az1" {
  vpc_id            = aws_vpc.development.id
  cidr_block        = "10.0.0.0/20"
  availability_zone = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "development-public-az1"
  }
}

resource "aws_subnet" "development_public_az2" {
  vpc_id            = aws_vpc.development.id
  cidr_block        = "10.0.16.0/20"
  availability_zone = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "development-public-az2"
  }
}

resource "aws_subnet" "development_private_az1" {
  vpc_id            = aws_vpc.development.id
  cidr_block        = "10.0.32.0/20"
  availability_zone = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "development-private-az1"
  }
}

resource "aws_subnet" "development_private_az2" {
  vpc_id            = aws_vpc.development.id
  cidr_block        = "10.0.48.0/20"
  availability_zone = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "development-private-az2"
  }
}