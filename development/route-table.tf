resource "aws_route_table" "development_public_route_table" {
  vpc_id = aws_vpc.development.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.development_igw.id
  }
}

resource "aws_route_table_association" "development_public_az1" {
  subnet_id      = aws_subnet.development_public_az1.id
  route_table_id = aws_route_table.development_public_route_table.id
}

resource "aws_route_table_association" "development_public_az2" {
  subnet_id      = aws_subnet.development_public_az2.id
  route_table_id = aws_route_table.development_public_route_table.id
}