resource "aws_subnet" "shared-public-az1" {
  vpc_id     = "${aws_vpc.shared.id}"
  cidr_block = "10.3.0.0/20"

  tags = {
    Name = "shared-public-az1"
  }
}

resource "aws_subnet" "shared-public-az2" {
  vpc_id     = "${aws_vpc.shared.id}"
  cidr_block = "10.3.16.0/20"

  tags = {
    Name = "shared-public-az2"
  }
}

resource "aws_subnet" "shared-private-az1" {
  vpc_id     = "${aws_vpc.shared.id}"
  cidr_block = "10.3.32.0/20"

  tags = {
    Name = "shared-private-az1"
  }
}

resource "aws_subnet" "shared-private-az2" {
  vpc_id     = "${aws_vpc.shared.id}"
  cidr_block = "10.3.48.0/20"

  tags = {
    Name = "shared-private-az2"
  }
}