

resource "aws_vpc" "shared" {
  cidr_block = "10.3.0.0/16"

  tags = {
    Name = "shared services"
  }
}
