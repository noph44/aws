provider "aws" {}

data "aws_caller_identity" "aws-identity" {}

data "aws_availability_zones" "available-zones" {
  state = "available"
}
