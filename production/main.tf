provider "aws" {}

data "aws_caller_identity" "aws_identity" {}

data "aws_availability_zones" "available" {
  state = "available"
}

terraform {
  backend "remote" {
    organization = "ParadigmTechnologies"

    workspaces {
      name = "aws-production"
    }
  }
}