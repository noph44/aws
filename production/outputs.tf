output "vpc" {
  value = aws_vpc.production
}

output "public_subnet_az1" {
  value = aws_subnet.production_public_az1
}

output "public_subnet_az2" {
  value = aws_subnet.production_public_az2
}

output "private_subnet_az1" {
  value = aws_subnet.production_private_az1
}

output "private_subnet_az2" {
  value = aws_subnet.production_private_az2
}

output "fargate_cluster" {
  value = aws_ecs_cluster.production_ecs_cluster
}

output "lb_listener" {
  value = aws_lb_listener.production_lb_listener
}

output "lb_sg" {
  value = aws_security_group.production_alb_sg
}