resource "aws_route_table" "production_public_route_table" {
  vpc_id = aws_vpc.production.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.production_igw.id
  }
}

resource "aws_route_table_association" "production_public_az1" {
  subnet_id      = aws_subnet.production_public_az1.id
  route_table_id = aws_route_table.production_public_route_table.id
}

resource "aws_route_table_association" "production_public_az2" {
  subnet_id      = aws_subnet.production_public_az2.id
  route_table_id = aws_route_table.production_public_route_table.id
}