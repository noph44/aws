

resource "aws_subnet" "production_public_az1" {
  vpc_id            = aws_vpc.production.id
  cidr_block        = "10.2.0.0/20"
  availability_zone = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "production-public-az1"
  }
}

resource "aws_subnet" "production_public_az2" {
  vpc_id            = aws_vpc.production.id
  cidr_block        = "10.2.16.0/20"
  availability_zone = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "production-public-az2"
  }
}

resource "aws_subnet" "production_private_az1" {
  vpc_id            = aws_vpc.production.id
  cidr_block        = "10.2.32.0/20"
  availability_zone = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "production-private-az1"
  }
}

resource "aws_subnet" "production_private_az2" {
  vpc_id            = aws_vpc.production.id
  cidr_block        = "10.2.48.0/20"
  availability_zone = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "production-private-az2"
  }
}