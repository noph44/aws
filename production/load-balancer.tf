resource "aws_lb" "production_alb" {
  name               = "production-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.production_alb_sg.id]
  subnets = [
    aws_subnet.production_public_az1.id,
    aws_subnet.production_public_az2.id
  ]

  tags = {
    Environment = "production"
  }

  depends_on = [aws_internet_gateway.production_igw]
}

resource "aws_lb_listener" "production_lb_listener" {
  load_balancer_arn = aws_lb.production_alb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = aws_acm_certificate_validation.production_cert_validation.certificate_arn

  default_action {
    type = "fixed-response"
    fixed_response {
      content_type = "text/plain"
      message_body = "Page not found"
      status_code  = 404
    }
  }
}

resource "aws_lb_listener" "production_http_lb_listener" {
  load_balancer_arn = aws_lb.production_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}