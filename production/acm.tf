resource "aws_acm_certificate" "production_cert" {
  domain_name               = "*.prd.thenoph.com"
  subject_alternative_names = ["*.thenoph.com"]
  validation_method         = "DNS"
}

resource "aws_acm_certificate_validation" "production_cert_validation" {
  certificate_arn         = aws_acm_certificate.production_cert.arn
  validation_record_fqdns = [for record in aws_route53_record.production_cert_validation_record : record.fqdn]
}