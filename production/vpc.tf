
resource "aws_vpc" "production" {
  cidr_block = "10.2.0.0/16"

  tags = {
    Name = "production"
  }
}
