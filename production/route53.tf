data "aws_route53_zone" "primary_zone" {
  zone_id = "ZB0FKTLM94EBG"
}

resource "aws_route53_zone" "production_zone" {
  name = "prd.thenoph.com"

  tags = {
    Environment = "prod"
  }
}

resource "aws_route53_record" "production_nameservers" {
  zone_id = data.aws_route53_zone.primary_zone.zone_id
  name    = "prd.thenoph.com"
  type    = "NS"
  ttl     = "30"

  records = [
    aws_route53_zone.production_zone.name_servers.0,
    aws_route53_zone.production_zone.name_servers.1,
    aws_route53_zone.production_zone.name_servers.2,
    aws_route53_zone.production_zone.name_servers.3,
  ]
}

resource "aws_route53_record" "production_load_balancer_record" {
  zone_id = aws_route53_zone.production_zone.zone_id
  name    = "*.prd.thenoph.com"
  type    = "A"

  alias {
    name                   = aws_lb.production_alb.dns_name
    zone_id                = aws_lb.production_alb.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "production_cert_validation_record" {
  for_each = {
    for dvo in aws_acm_certificate.production_cert.domain_validation_options : dvo.domain_name => {
      name    = dvo.resource_record_name
      record  = dvo.resource_record_value
      type    = dvo.resource_record_type
      zone_id = dvo.domain_name == "*.prd.thenoph.com" ? aws_route53_zone.production_zone.zone_id : data.aws_route53_zone.primary_zone.zone_id
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = each.value.zone_id
  # zone_id         = aws_route53_zone.production_zone.zone_id
}