

resource "aws_subnet" "stage-public-az1" {
  vpc_id     = "${aws_vpc.stage.id}"
  cidr_block = "10.1.0.0/20"

  tags = {
    Name = "stage-public-az1"
  }
}

resource "aws_subnet" "stage-public-az2" {
  vpc_id     = "${aws_vpc.stage.id}"
  cidr_block = "10.1.16.0/20"

  tags = {
    Name = "stage-public-az2"
  }
}

resource "aws_subnet" "stage-private-az1" {
  vpc_id     = "${aws_vpc.stage.id}"
  cidr_block = "10.1.32.0/20"

  tags = {
    Name = "stage-private-az1"
  }
}

resource "aws_subnet" "stage-private-az2" {
  vpc_id     = "${aws_vpc.stage.id}"
  cidr_block = "10.1.48.0/20"

  tags = {
    Name = "stage-private-az2"
  }
}