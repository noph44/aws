output "vpc" {
    value = "${aws_vpc.stage}"
}

output "public-subnet-az1" {
    value = "${aws_subnet.stage-public-az1}"
}

output "public-subnet-az2" {
    value = "${aws_subnet.stage-public-az2}"
}

output "private-subnet-az1" {
    value = "${aws_subnet.stage-private-az1}"
}

output "private-subnet-az2" {
    value = "${aws_subnet.stage-private-az2}"
}