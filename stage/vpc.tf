resource "aws_vpc" "stage" {
  cidr_block = "10.1.0.0/16"

  tags = {
    Name = "stage"
  }
}